<?php

use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\CompanyController::class, 'index']);
Route::get('/company/tambah', [\App\Http\Controllers\CompanyController::class, 'tambah']);
Route::post('/company/store', [\App\Http\Controllers\CompanyController::class, 'store']);
Route::get('/company/edit/{id}',[\App\Http\Controllers\CompanyController::class, 'edit']);
Route::post('/company/update/{id}', [\App\Http\Controllers\CompanyController::class, 'update']);
Route::get('/company/delete/{id}', [\App\Http\Controllers\CompanyController::class, 'delete']);
