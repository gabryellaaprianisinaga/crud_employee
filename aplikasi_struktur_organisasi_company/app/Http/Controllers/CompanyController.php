<?php

namespace App\Http\Controllers;

use App\Models\Company;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    // method untuk menampilkan data company
    public function index(){
        $company= Company::all();
        return view('data_company', ['company' => $company]);
    }

    // method untuk menampilkan halaman menambah data company
    public function tambah (){
        return view('tambah_data');
    }

    // method untuk menyimpan data yang baru saja ditambahkan
    public function store(Request $request){

        DB::table('company')->insert([
            'id' => $request->id,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
        ]);

        return redirect('/');
    }

    public function edit($id){
        $company= Company::find($id);
        return view('company_edit', ['company' => $company]);
    }

    public function update(Request $request){
        DB::table('company')->where("id",$request->id)->update([
            'id' => $request->id,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
        ]);
        return redirect('/');
    }

    public function delete($id){
        $company = Company::find($id);
        $company->delete();
        return redirect('/');
    }
}
