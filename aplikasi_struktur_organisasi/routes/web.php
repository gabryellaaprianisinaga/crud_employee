<?php

use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [EmployeeController::class, 'index']);
Route::get('/karyawan/tambah', [EmployeeController::class, 'tambah']);
Route::post('/karyawan/store', [EmployeeController::class, 'store']);
Route::get('/karyawan/edit/{id}',[EmployeeController::class, 'edit']);
Route::post('/karyawan/update/{id}', [EmployeeController::class, 'update']);
Route::get('/karyawan/delete/{id}', [EmployeeController::class, 'delete']);
