<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    // method untuk menampilkan data employee
    public function index(){
        $karyawan = Employee::all();
        return view('data_karyawan', ['karyawan' => $karyawan]);
    }

    // method untuk menampilkan halaman menambah data employe
    public function tambah (){
        return view('tambah_data');
    }

    // method untuk menyimpan data yang baru saja ditambahkan
    public function store(Request $request){

        DB::table('employee')->insert([
            'id' => $request->id,
            'nama' => $request->nama,
            'atasan_id' => $request->atasan_id,
            'company_id' => $request->company_id,
        ]);

        return redirect('/');
    }

    public function edit($id){
        $karyawan = Employee::find($id);
        return view('karyawan_edit', ['karyawan' => $karyawan]);
    }

    public function update(Request $request){
        DB::table('employee')->where("id",$request->id)->update([
            'id' => $request->id,
            'nama' => $request->nama,
            'atasan_id' => $request->atasan_id,
            'company_id' => $request->company_id,
        ]);
        return redirect('/');
    }

    public function delete($id){
        $karyawan = Employee::find($id);
        $karyawan->delete();
        return redirect('/');
    }
}
